
Created/written terraform script (you can grab from ecr-tf file)

Written terraform script to create ecr repo, esc cluster
This Terraform script will also deploys EC2 on AWS, installs git, Jenkins, and docker and run Jenkins and docker and clone ECR repo from GIT  

HOW TO USE :

Go to gitlab repo (mani639-ecr) and make a commit 
        you will see ECS cluster on AWS and docker-file has built and pushed to ECR using Jenkins  

How To Build:

NOTE: we are using built S3 bucket (created when we run mani-639 terraform(jenkins-tf)).

Pre-Requisite: create Access token in GITLAB 
        click on user icon in top left -> select preferences -> Access tokens 
                provide token name, expiration and select all scopes and create( copy and save the token)
        

Copy the code from ecr-tf file in the repo and deploy/ terraform apply from your local then you will ECR repository built on AWS.modify variable according to your AWS account)
Copy the code from ecs-tf file in the repo and deploy/ terraform apply from your local then you will ECS cluster built on AWS.(modify variable according to your AWS account) you will see instance deployed in AWS console and cluster in ECS

login to EC2 instance and run "sudo chown -R jenkins: ." command
Then use this public IP to login to Jenkins using 8080 port (publicIP:8080). you will see a page asking to create User, create user and you now successfully logged into Jenkins.

Now you have to configure Jenkins:

First you should install GITLAB plug-in from manage Jenkins -> manage plugins 

        1. Under Manage Jenkins -> configure system -> scroll down to GITLAB and provide info below
            Name: GIT lab
            Gitlab HOST URL: https://gitlab.com/
            credentials: 
                    click on add -> select Jenkins from dropdown box and provide info:
                        Kind: Gitlab API token
                        API Token: provide API token created in GITLAB
                        ID : GITLab API toeken 

        2. Storing GITLAB account password in JENKINS credentials :
         under credential store -> click on Jenkins -> Global Credentials ->  Add credentials ->select secret text from dropdown  box add GIT Lab username and password
        3. Then go to Dash board create new Item :
                Item Name: provide project name
                select pipeline and save and use below steps to configure
                    Under build triggers: select build when change is pushed to GITLAB. 
                    copy pipeline script from Jenkins file in repo and paste in pipeline script.
        

Steps to do in GIT LAB:
  Open repo(mani-639) -> Integrations -> select Jenkins and configure
        provide Jenkins URL (https://publicIP:8080/).
        project name: "Jenkins project name"
        username:"Jenkins USER name"
        password: "Jenkins password"

Now you have setup everything and follow steps under How to use this built project to see output.
