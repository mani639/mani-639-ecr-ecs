variable "region" {
   default = "us-east-2"
}
variable "namespace"{
   default = "mani"
}

variable "stage"{
   default = "kajjayam"
}

variable "ecr_name"{
   default = "mani639"
}

variable "ecs_name"{
   default = "mani693-ecs"
}

variable "capacity_providers"{
   type    = list(string)
   default = ["FARGATE"]
}

variable "Environment"{
   default = "mani-dev"
}

variable "fargate_name_prefix"{
   default = "catweb"
}

variable "container_name"{
   default = "catweb"
}

variable "container_image"{
   default = "720253322549.dkr.ecr.us-east-2.amazonaws.com/catweb:latest"
}

variable "containerPort"{
   default = 5000
}

variable "hostPort"{
   default = 5000
}

variable "protocol"{
   default = "tcp"
}

variable "fargate_schedule_name_prefix"{
   default = "mani639-scheduled-task"
}

variable "ecs_cluster_arn"{
   default = "arn:aws:ecs:us-east-2:720253322549:cluster/my-ecs"
}

variable "task_role_arn"{
   default = "arn:aws:iam::720253322549:role/mani639test-ecs-task-execution-role"
}

variable "execution_role_arn"{
   default = "arn:aws:iam::720253322549:role/mani639test-ecs-task-execution-role"
}

variable "event_target_task_definition_arn"{
   default = "arn:aws:ecs:us-east-2:720253322549:task-definition/mani639test-td:2"
}

variable "event_rule_schedule_expression"{
   default = "rate(1 minute)"
}

variable "event_target_task_count"{
   default = 1
}

variable "event_target_subnets"{
   type    = list(string)
   default = ["subnet-07b4621fab6b2b1f8"]
}

variable "dynamodb_name"{
   default = "iac-terraform-state-lock-dynamo-ecr"
}
