resource "aws_dynamodb_table" "dynamodb-terraform-state-lock" {
  name = "${var.dynamodb_name}"
  hash_key = "LockIDecr"
  read_capacity = 20
  write_capacity = 20

  attribute {
    name = "LockIDecr"
    type = "S"
  }
}
